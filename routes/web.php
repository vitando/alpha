<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('welcome');

// Adminarea

Route::get('/adminarea', 'AdminController@index')->name('admin');
Route::post('/categorie/create', 'AdminController@createCategorie');
Route::post('/aim/create', 'AdminController@createAim');

// Userarea

Route::get('/userarea', 'UserareaController@index')->name('home');

Route::get('/userarea/rezepte', 'UserareaController@recipes')->name('homeRecipes');
Route::get('/userarea/übungen', 'UserareaController@exercises')->name('homeExercises');
Route::get('/userarea/ernährungspläne', 'UserareaController@foodplans')->name('homeFoodplans');
Route::get('/userarea/trainingspläne', 'UserareaController@trainplans')->name('homeTrainplans');
Route::get('/userarea/kalender', 'UserareaController@clndr')->name('homeClndr');
Route::get('/userarea/profile', 'UserareaController@profile')->name('homeProfile');


// User

Route::post('/userarea/aim', 'UserController@aim');
Route::get('/userarea/events/get', 'UserController@getEvents');


// Ernährungspläne

Route::post('/foodplan/store', 'FoodplanController@store');
Route::get('/ernährungsplan/erstellen', 'FoodplanController@create')->name('createFoodplan');
Route::get('/ernährungsplan/bearbeiten', 'FoodplanController@edit')->name('editFoodplan');
Route::get('/ernährungsplan/bearbeiten/{id}', function ($id) {
    return redirect()->route('editFoodplan')->with('id', $id);
});
Route::patch('/ernährungsplan/bearbeiten/{foodplan}', 'FoodplanController@update');

// Trainingspläne

Route::post('/trainplan/store', 'TrainplanController@store');
Route::get('/trainingsplan/erstellen', 'TrainplanController@create')->name('createTrainplan');
Route::get('/trainingsplan/bearbeiten', 'TrainplanController@edit')->name('editTrainplan');
Route::get('/trainingsplan/bearbeiten/{id}', function ($id) {
    return redirect()->route('editTrainplan')->with('id', $id);
});
Route::patch('/trainingsplan/bearbeiten/{trainplan}', 'TrainplanController@update');

// Rezepte

Route::post('/recipe/store', 'RecipeController@store');
Route::get('/rezept/erstellen', 'RecipeController@create')->name('createRecipe');
Route::get('/rezept/bearbeiten', 'RecipeController@edit')->name('editRecipe');
Route::get('/rezept/bearbeiten/{id}', function ($id) {
    return redirect()->route('editRecipe')->with('id', $id);
});
Route::patch('/rezept/bearbeiten/{recipe}', 'RecipeController@update');

// Übungen

Route::post('/exercise/store', 'ExerciseController@store');
Route::get('/übung/erstellen', 'ExerciseController@create')->name('createExercise');
Route::get('/übung/bearbeiten', 'ExerciseController@edit')->name('editExercise');
Route::get('/übung/bearbeiten/{id}', function ($id) {
    return redirect()->route('editExercise')->with('id', $id);
});
Route::patch('/übung/bearbeiten/{exercise}', 'ExerciseController@update');

// für jquery

Route::get('/food/find', 'RecipeController@findFood');
Route::get('/recipe/by_categorie', 'RecipeController@findRecipe');
Route::get('/exercise/by_categorie', 'ExerciseController@findExercise');
