<!-- !!! Rezept bearbeiten !!! -->
<form action="/rezept/bearbeiten/{{$recipe->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Rezept bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$recipe->title}}" placeholder="{{$recipe->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Kategorie</label>
        

    </div>  

    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$recipe->description}}</textarea>
    </div>  

    <div class="row text-center">
        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
            <label for="">Zutat</label>
        </div>

        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <label for="">Menge</label>
        </div>

        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            <label for=""><br></label>
        </div>
    </div>       
    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
    
        @foreach($recipe->foods as $food)    
        <div class="row form-group ingredients">

            <input name="food[]"  type="hidden" class="form-control new-day-id" value="{{$food->id}}">

            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 magic-reset">
                <input name="food[]" type="text" class="form-control magic-food"  value="{{$food->food->id}}" required="required">    
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input name="food[]" type="number" class="form-control change-day-h" value="{{$food->amount}}" required="required">  
            </div>
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <button type="button" class="btn btn-default pull-right glyphicon glyphicon-remove delete-insert"></button>
            </div>
        </div>
        @endforeach
        <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus add-food"></button>
    </div>
                  
    <button type="submit" class="btn btn-primary">Speichern</button>
</form>