<!-- !!! Rezept erstellen !!! -->
<form action="/recipe/store" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <label for="">Titel</label>
            <input type="text" class="form-control" name="title" placeholder="" required="required">
            <label for="">Beschreibung</label>
            <textarea name="description" class="form-control" rows="5" required="required"></textarea>
        </div> 
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label for="">Ziele</label> <br>
                    @foreach($aims as $aim)
                        <span class="form-group btn btn-danger toggleAim">
                            <input type="hidden" name="" class="form-control" value="{{$aim->id}}">
                           {{$aim->title}}
                        </span>
                    @endforeach 
            </div>                     
            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label for="">Kategorie</label> <br>
                    @foreach($recipes_categories as $recipes_categorie)
                        <span class="form-group btn btn-danger toggleCategorie">
                            <input type="hidden" name="" class="form-control" value="{{$recipes_categorie->id}}">
                           {{$recipes_categorie->title}}
                        </span>
                    @endforeach
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
            <label for="">Zutat</label>
        </div>

        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <label for="">Menge</label>
        </div>

        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            <label for=""><br></label>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
            <div class="row form-group ingredients">
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 magic-reset">
                    <input type="text" class="form-control magic-food" name="food[]" required="required">    
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <input type="number" class="form-control" name="food[]" placeholder="" min="1" required="required">  
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <button type="button" class="btn btn-default pull-right glyphicon glyphicon-remove delete-div" disabled></button>
                </div>
            </div>
            <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus add-food"></button>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Rezept erstellen!</button>
</form>
