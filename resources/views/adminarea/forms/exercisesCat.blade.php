<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Kategorie für Übungen erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/categorie/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="type" id="inputType" class="form-control" value="exercise">
            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required>
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Hauptkategorie</label>
                <select name="scope" class="form-control" required="required">
                    <option value="0" selected>Ist eine Hauptkategorie</option>
                    @foreach($exercises_categories as $exercises_categorie)
                            <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required" required></textarea>
            </div>                    
            <button type="submit" class="btn btn-primary pull-right">Erstellen</button>
        </form>
    </div>
</div>