@extends('adminarea.master')

@section('areacontent')
<div class="topspace"></div>
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            @include('adminarea.forms.recipesCat')
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            @include('adminarea.forms.exercisesCat')
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            @include('adminarea.forms.foodplansCat')
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            @include('adminarea.forms.trainplansCat')
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('adminarea.forms.aims')
        </div>
    </div>
</div>
<br><br>

@endsection
