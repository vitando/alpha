@extends('layouts.master')

@section('head')
    @include('layouts.head_userarea')
@endsection


@section('content')
    <div class="topspace"></div>
    @include('modules.createNav') <br>
    @yield('areacontent')

@endsection
