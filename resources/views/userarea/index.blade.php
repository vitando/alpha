@extends('userarea.master')

@section('areacontent')
            
        <!-- team -->
        <section id="team" class="team-section appear clearfix">
        <div class="container">

                <div class="row mar-bot10">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="section-header">
                        <div class="wow bounceIn">
                        
                            <h2 class="section-heading animated" data-animation="bounceInUp">Jetz beginnen!</h2>
                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit, sed quia non numquam.</p>
                        
                        </div>
                        </div>
                    </div>
                </div>

                    <div class="row align-center mar-bot45">
                        <div class="col-md-6">
                        <div class="wow bounceIn" data-animation-delay="4.8s">
                            <div class="team-member">
                                <div class="profile-picture">
                                    <!-- <figure><img src="img/members3.jpg" alt=""></figure> -->
                                    <div class="profile-overlay"></div>
                                        <div class="profile-social">
                                            <div class="icons-wrapper">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                            </div>
                                        </div>
                                </div>
                                <div class="team-detail">
                                    <h1><a href="/userarea/profile">Ziel festlegen!</a></h1>
                                    <span>Info</span>
                                </div>
                                <div class="team-bio">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, fugiat, harum, adipisci accusantium minus asperiores.</p>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                            
                            <div class="wow bounceIn">
                            <div class="team-member">
<!--                                 <div class="profile-picture">
                                    <figure><img src="img/members1.jpg" alt=""></figure>
                                    <div class="profile-overlay"></div>
                                        <div class="profile-social">
                                            <div class="icons-wrapper">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                            </div>
                                        </div>
                                </div> -->
                                <div class="team-detail">
                                    <h1><a href="">Daten vervollständigen!</a></h1>
                                    <span>Info</span>
                                </div>
                                <div class="team-bio">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, fugiat, harum, adipisci accusantium minus asperiores.</p>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                    </div>
                        
        </div>
        </section>
        <!-- /team -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4 col-xs-offset-2 col-sm-4 col-md-4 col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Dein Profil<a href="/userarea/profile">Beabreiten</a></h3>
                    </div>
                    <div class="panel-body">
                        Nick: {{ Auth::user()->name }} <br>
                        E-Mail: {{ Auth::user()->email }} <br>  
                        Gewicht in Kilogramm: {{ Auth::user()->weight }} <br>
                        Größe in Zentimetern: {{ Auth::user()->size }} <br>
                        @if(!empty(Auth::user()->aim))
                        Ziel: {{ Auth::user()->aim->show->title }} <br>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Dein heutiger Plan</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>
            </div>
        </div>
    </div>
            
    <!-- Dein Bereich -->
    <section id="services" class="section pad-bot5 bg-white">
    <div class="container"> 
            <div class="row mar-bot5">
                <div class="col-md-offset-3 col-md-6">
                    <div class="section-header">
                    <div class="wow bounceIn"data-animation-delay="7.8s">
                    
                        <h2 class="section-heading animated">Dein Bereich</h2>
                        <h4>Hier kannste Du alle deine erstellten und gelikten Trainingspläne, Ernährungspläne, Rezepte und Übungen managen.</h4>
                    
                    </div>
                    </div>
                </div>
            </div>
        <div class="row mar-bot40">
            <div class="col-lg-4" >
                <div class="wow bounceIn">
                    <div class="align-center">
                
                        <div class="wow rotateIn">
                            <div class="service-col">
                                <div class="service-icon">
                                    <figure><i class="fa fa-cutlery"></i></figure>
                                </div>
                                    <h2>Ernährungspläne</h2>
                                    <p>
                                        <ul class="list-group">
                                            <a href="/userarea/ernährungspläne"><li class="list-group-item"><i class="fa fa-user" aria-hidden="true"></i> Eigene</li></a>
                                            <a href=""><li class="list-group-item"><i class="fa fa-search" aria-hidden="true"></i> Suchen</li></a>
                                            <a href="/ernährungsplan/erstellen"><li class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i> Erstellen</li></a>
                                        </ul>
                                    </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="col-lg-4" >
                <div class="align-center">
                    <div class="wow bounceIn">
                   
                        <div class="wow rotateIn">
                            <div class="service-col">   
                                <div class="service-icon">
                                    <figure><i class="fa fa-rocket"></i></figure>
                                </div>
                                    <h2>Trainingspläne</h2>
                                    <p>
                                        <ul class="list-group">
                                            <a href="/userarea/trainingspläne"><li class="list-group-item"><i class="fa fa-user" aria-hidden="true"></i> Eigene</li></a>
                                            <a href=""><li class="list-group-item"><i class="fa fa-search" aria-hidden="true"></i> Suchen</li></a>
                                            <a href="/trainingsplan/erstellen"><li class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i> Erstellen</li></a>
                                        </ul>
                                    </p>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        
            <div class="col-lg-4" >
                <div class="align-center">
                    <div class="wow bounceIn">
                        <div class="service-col">
                            <div class="service-icon">
                                <figure><i class="fa fa-fire"></i></figure>
                            </div>
                                <h2>Rezepte & Übungen</h2>
                                <p>
                                    <ul class="list-group">
                                        <a href="/rezept/erstellen"><li class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i> Rezepte Erstellen</li></a>
                                        <a href="/userarea/rezepte"><li class="list-group-item"><i class="fa fa-user" aria-hidden="true"></i> Deine Rezepte</li></a>
                                        <a href="/übung/erstellen"><li class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i> Übungen Erstellen</li></a>
                                        <a href="/userarea/übungen"><li class="list-group-item"><i class="fa fa-user" aria-hidden="true"></i> Deine Übungen</li></a>
                                    </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        
        </div>  

    </div>
    </section>
    <!--/Dein Bereich-->
    <!--Kalender-->
    <section id="section-about">
        <div class="container">
            <div class="about">
                <div class="row mar-bot40">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="title">
                            <div class="wow bounceIn">
                        
                            <h2 class="section-heading animated" data-animation="bounceInUp"><a href="/userarea/kalender">Dein Kalender</a></h2>
                            
                        
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
            
                    <div class="row-slider">
                        <div class="col-lg-6 mar-bot30">
                            <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                                <div class="slides" data-group="slides">
                                    <ul>
                
                                        <div class="slide-body" data-group="slide">
                                            <li><img alt="" class="img-responsive" src="img/9.jpg" width="100%" height="350"/></li>
                                            <li><img alt="" class="img-responsive" src="img/10.jpg" width="100%" height="350"/></li>
                                            <li><img alt="" class="img-responsive" src="img/11.jpg" width="100%" height="350"/></li>
                            
                                        </div>
                                    </ul>
                                        <a class="slider-control left" href="#" data-jump="prev"><i class="fa fa-angle-left fa-2x"></i></a>
                                        <a class="slider-control right" href="#" data-jump="next"><i class="fa fa-angle-right fa-2x"></i></a>
                                
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-lg-6 ">
                            <div class="company mar-left10">
                                <h4>Our Company has created 1928 morbi leo risus, porta ac consectetur ac, <span>vestibulum </span> at eros.</h4>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Donec sed odio dui. Fusce dapibus, tellus ac cursus etiam porta sem malesuada magna mollis euismod. commodo, Faccibus mollis interdum. Morbi leo risus, porta ac, vestibulum at eros.
                                  Nullam id dolor id nibh ultricies vehicula ut id elit. Donec sed odio dui. Fusce dapibus, tellus ac.</p>
                            </div>
                            <div class="list-style">
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li>Sollicitudin Vestibulum</li>
                                            <li>Fermentum Pellentesque</li>
                                            <li>Sollicitudin Vestibulum</li>
                                            <li>Nullam id dolor id nibh</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li>Sollicitudin Vestibulum</li>
                                            <li>Fermentum Pellentesque</li>
                                            <li>Sollicitudin Vestibulum</li>
                                            <li>Nullam id dolor id nibh</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>  
                </div>
                    
            </div>
            
        </div>
    </section>
    <!--/Kalender-->

    <!-- @include('modules.forms.createPal') -->
@endsection