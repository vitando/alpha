@extends('userarea.master')

@section('areacontent')
<div class="row">
    <div class="col-xs-4 col-xs-offset-2 col-sm-4 col-md-4 col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Deine Ernährunspläne</h3>
            </div>
            <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Titel</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($foodplans as $foodplan)
                                <tr>
                                    <td>{{$foodplan->title}}</td>
                                    <td><a href="/ernährungsplan/bearbeiten/{{$foodplan->id}}">Bearbeiten</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>
@include('modules.createNav')
@endsection
