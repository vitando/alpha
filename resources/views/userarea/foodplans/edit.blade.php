@extends('userarea.master')

@section('areacontent')
<section id="section-contact" class="section appear clearfix">
        <div class="container">
            <div class="row mar-bot40">
                <div class="col-md-12">
                    <div class="section-header">
                        <h2 class="section-heading animated" data-animation="bounceInUp">Bearbeite deinen Ernährungsplan</h2>
                        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit, sed quia non numquam.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
					@include('foodplan.forms.edit')
                </div>
                <!-- ./span12 -->
            </div>
            
        </div>
</section>

@endsection
