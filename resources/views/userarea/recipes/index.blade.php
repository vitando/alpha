@extends('userarea.master')

@section('areacontent')
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dein erstellten Rezepte</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Titel</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($recipes as $recipe)
                                <tr>
                                    <td>{{$recipe->title}}</td>
                                    <td><a href="/rezept/bearbeiten/{{$recipe->id}}">Bearbeiten</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Rezepte für heute vorgemerkt</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
