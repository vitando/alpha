@extends('userarea.master')
@section('areacontent')
<div class="container">
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dein Profil bearbeiten</h3>
                </div>
                <div class="panel-body">
                    <form action="/userarea/aim" method="POST" role="form">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="">Gewicht</label>
                            <input type="number" class="form-control" name="weight" value="@if(!empty(Auth::user()->aim)){{ Auth::user()->aim->weight }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="">Notiz</label>
                            <input type="text" class="form-control" name="note" value="@if(!empty(Auth::user()->aim)) {{ Auth::user()->aim->note }} @endif">
                        </div>
                        <button type="submit" class="btn btn-primary">Speichern!</button>
                    </form> 
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dein Ziel bearbeiten</h3>
                </div>
                <div class="panel-body">
                    <form action="/userarea/aim" method="POST" role="form">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="">Ziel</label>
                            <select name="aim_id" class="form-control" required="required">
                                @foreach($aims as $aim)
                                    @if(!empty(Auth::user()->aim->id) && $aim->aim_id == Auth::user()->aim->id)
                                    <option value="{{ $aim->id }}" selected>{{ $aim->title }}</option>
                                    @endif
                                    <option value="{{ $aim->id }}">{{ $aim->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Gewichtsveränderung</label>
                            <input type="number" class="form-control" name="weight" value="@if(!empty(Auth::user()->aim)){{ Auth::user()->aim->weight }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="">Notiz</label>
                            <input type="text" class="form-control" name="note" value="@if(!empty(Auth::user()->aim)) {{ Auth::user()->aim->note }} @endif">
                        </div>
                        <button type="submit" class="btn btn-primary">Speichern!</button>
                    </form> 

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
