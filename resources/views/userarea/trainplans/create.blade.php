@extends('userarea.master')

@section('areacontent')
<section id="section-contact" class="section appear clearfix">
        <div class="container">
            <div class="row mar-bot40">
                <div class="col-md-12">
                    <div class="section-header">
                        <h2 class="section-heading animated" data-animation="bounceInUp">Erstelle deinen eigenen Trainingsplan</h2>
                        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit, sed quia non numquam.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    
                    @include('trainplan.forms.create')
                    
                </div>
                <!-- ./span12 -->
            </div>
            
        </div>
</section>

<div class="topspace"></div>
<!-- !!! Übung erstellen !!! -->
<div class="container text-center">
    <!-- Button Übung modal -->
    <button type="button" class="btn btn-theme btn-md" data-toggle="modal" data-target="#myModal">
      Neue Übung anlegen!
    </button> <br><br>
    <!-- Modal Übung anlegen -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Übung erstellen!</h4>
                </div>
                    <div class="container-fluid">
                        @include('exercise.forms.create')
                    </div>
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Schließen (ohne zu Speichern)</button>
                <div class="text-center">Eventuell musst Du die Kategorien wechseln um das Rezept neu zu laden!</div>
            </div>
        </div>
    </div>      

</div>

@endsection
