@extends('userarea.master')
@section('areacontent')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Übung bearbeiten: {{ $exercise->title }}</h3>
                </div>
                <div class="panel-body">
                    @include('exercise.forms.edit')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
