@extends('userarea.master')
@section('areacontent')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dein Kalender</h3>
                </div>
                <div class="panel-body">



                  <script type="text/template" id="swag">
                          <div class='clndr-controls'>
                            <div class="row">
                              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-2">
                                <span class='clndr-control-button'>
                                    <span class='clndr-previous-button glyphicon glyphicon-arrow-left'></span>
                                </span>
                              </div>
                              
                              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <span class='month'><%= month %> <%= year %></span>
                              </div>
                              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <span class='clndr-control-button rightalign'>
                                    <span class='clndr-next-button glyphicon glyphicon-arrow-right'></span>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                              <table class='cal-body' border='0' cellspacing='0' cellpadding='0'>
                                <thead>
                                    <tr class='header-days'>
                                    <% for(var i = 0; i < daysOfTheWeek.length; i++) { %>
                                        <td class='header-day'><h4><%= daysOfTheWeek[i] %></h4></td>
                                    <% } %>
                                    </tr>
                                </thead>
                                <tbody>
                                <% for(var i = 0; i < numberOfRows; i++){ %>
                                    <tr class="cal-day">
                                    <% for(var j = 0; j < 7; j++){ %>
                                    <% var d = j + i * 7; %>
                                    <% var time = days[d].date + 86400000; %>
                                    <% var day = new Date(time).toISOString(); %>
                                    <% var date = day.substring(0, 10); %>

                                        <td class='<%= days[d].classes %>'>
                                            <div class='day-content'><%= days[d].day %></div>
                                              <% _.each(eventsThisMonth, function(event) { %>
                                                <% if ( event.date === date ) { %>
                                                  <div class="event-item">
                                                  <span href="/clndr/events/<%= event.type %>/<%= event.id %>" data-event-id="<%= event.eventID %>" data-id="<%= event.id %>"  data-name="<%= event.title %>" data-type="<%= event.type %>" data-date="<%= event.date %>" data-toggle="modal" data-target="#clndrModal" class="btn btn-success event-post"><%= event.title %></span><span class="btn btn-danger delete-event glyphicon glyphicon-remove"></span>
                                                  </div>
                                                <% } %>
                                              <% }); %>
                                        </td>
                                    <% } %>
                                    </tr>
                                <% } %>
                                </tbody>
                              </table>
                          </div>
                  </script>

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <button type="button" class="btn btn-success update-clndr">Kalender speichern!</button>
                      <!-- Kalender anzeigen -->
                    <div id="cal1"></div>

                  </div>

                  <!-- Modal -->
                  <div class="modal fade" id="clndrModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                          </div>
                      </div>
                  </div> 



                </div>
            </div>
        </div>

    </div>
</div>
@endsection
