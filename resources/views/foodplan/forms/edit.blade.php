@php ($old_day = $foodplan->recipes->first()->day)

<!-- !!! Ernährungspläne bearbeiten !!! -->

<form action="/ernährungsplan/bearbeiten/{{$foodplan->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Ernährungsplan bearbeiten</legend>
    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$foodplan->title}}" placeholder="{{$foodplan->title}}">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Kategorie</label>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$foodplan->description}}</textarea>
    </div>                    
    
    <!-- Hier fängt der Tag an! -->

    @foreach($foodplan->recipes as $recipe)
        @if($loop->first)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
            <div class="row form-group">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label for="">Name für Tag: *</label>
                    <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" value="{{$recipe->day}}" required>
                    </div>
                <button type="button" class="btn btn-default pull-right delete-div" disabled>Tag löschen</button>
                <button type="button" class="btn btn-default pull-right add-plan-day">Tag kopieren</button>
            </div>
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Kategorie</label>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label for="">Rezept</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Menge</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Beschreibung / Notiz</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for=""><br></label>
                </div>                                
            </div>
        @endif
        @if($recipe->day != $old_day)
            <button type="button" class="btn btn-default glyphicon glyphicon-plus insert"></button> Rezept hinzufügen! <br> <br>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
            <div class="row form-group">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label for="">Name für Tag: *</label>
                    <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" value="{{$recipe->day}}" required>
                    </div>
                <button type="button" class="btn btn-default pull-right delete-div">Tag löschen</button>
                <button type="button" class="btn btn-default pull-right add-plan-day">Tag kopieren</button>
            </div>            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Kategorie</label>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label for="">Rezept</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Menge</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Beschreibung / Notiz</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for=""><br></label>
                </div>                                
            </div>
            <div class="row form-group plan-insert">
            <!--  Hier werden die Kategorien für die Übungen dargestellt -->
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select id="recipe" class="form-control insert-categorie">
                        <option value="">Bitte auswählen!</option>
                        @foreach($recipes_categories as $recipes_categorie)
                                <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                        @endforeach
                    </select>
                </div>
                <!--  Hier werden die Übungen dargestellt -->
                <input name="recipes[]" type="hidden" class="form-control new-day-id" value="{{$recipe->id}}">
                <input name="recipes[]" type="hidden" class="form-control change-day-h" value="{{$recipe->day}}">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <select name="recipes[]" class="form-control insert-with-categorie" required="required" readonly><option value="{{ $recipe->recipe()->first()->id }}">{{ $recipe->recipe()->first()->title }}</option></select>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="number" class="form-control" value="{{ $recipe->amount }}">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="text" class="form-control" value="{{ $recipe->description }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-insert" disabled></button>
                </div>
            </div>
            @php ($old_day = $recipe->day)
        @else        
            <div class="row form-group plan-insert">
                <!--  Hier werden die Kategorien für die Übungen dargestellt -->
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select id="recipe" class="form-control insert-categorie">
                        <option value="">Bitte auswählen!</option>
                        @foreach($recipes_categories as $recipes_categorie)
                                <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                        @endforeach
                    </select>
                </div>
                <!--  Hier werden die Übungen dargestellt -->
                <input name="recipes[]" type="hidden" class="form-control new-day-id" value="{{$recipe->id}}">
                <input name="recipes[]" type="hidden" class="form-control change-day-h" value="{{$recipe->day}}">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <select name="recipes[]" class="form-control insert-with-categorie" required="required" readonly><option value="{{ $recipe->recipe()->first()->id }}">{{ $recipe->recipe()->first()->title }}</option></select>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="number" class="form-control" value="{{ $recipe->amount }}">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="text" class="form-control" value="{{ $recipe->description }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-insert"></button>
                </div>
            </div>
            @php ($old_day = $recipe->day)
        @endif
    @endforeach

        <button type="button" class="btn btn-default glyphicon glyphicon-plus insert"></button> Rezept hinzufügen! <br> <br>
    </div> 
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button type="submit" class="btn btn-primary">Ernährungsplan speichern!</button>
         
     </div> 

</form>