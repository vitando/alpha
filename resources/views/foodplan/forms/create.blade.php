<form action="/foodplan/store" method="POST" id="foodplan" role="form">
    <input name="_token" type="hidden" value="{{ csrf_token() }}">
    <div class="row">
        <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <label for="">Titel *</label>
            <input name="title" type="text" class="form-control" placeholder="">
            <label for="">Beschreibung *</label>
            <textarea name="description" class="form-control" rows="5" required="required"></textarea>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label for="">Ziele *</label> <br>
                    @foreach($aims as $aim)
                        <span class="form-group btn btn-danger toggleAim">
                            <input type="hidden" name="" class="form-control" value="{{$aim->id}}">
                           {{$aim->title}}
                        </span>
                    @endforeach 
            </div> 
            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label for="">Kategorien *</label> <br>
                @foreach($foodplans_categories as $foodplans_categorie)
                    <span class="form-group btn btn-danger toggleCategorie">
                        <input type="hidden" name="" class="form-control" value="{{$foodplans_categorie->id}}">
                       {{$foodplans_categorie->title}}
                    </span>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Hier fängt der Tag an! -->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
        <div class="row form-group">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Name für Tag: *</label>
                <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" required>
            </div>
            <button type="button" title="Tag löschen" class="btn btn-default pull-right delete-div" disabled>Tag löschen</button>
            <button type="button" title="Tag kopieren" class="btn btn-default pull-right add-plan-day">Tag kopieren</button>
        </div>
        <div class="row form-group">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="">Kategorie</label>
            </div>
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                <label for="">Rezept *</label>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label title="" for="">Menge *</label>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="">Beschreibung / Notiz</label>
            </div>
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <label for=""><br></label>
            </div>                                
        </div>
        <!--  Hier werden die Kategorien für die Rezepte dargestellt -->
        <div class="row form-group plan-insert">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <select id="recipe" class="form-control insert-categorie" required="required">
                    <option value="">Bitte auswählen!</option>
                    @foreach($recipes_categories as $recipes_categorie)
                            <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                    @endforeach
                </select>
            </div>
            <!--  Hier werden die Rezepte dargestellt -->
            <input name="recipes[]" type="hidden" class="form-control change-day-h" value="0">
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                <select name="recipes[]" value="0" class="form-control insert-with-categorie" required="required"><option value="">Bitte auswählen!</option></select>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <input name="recipes[]" type="number" class="form-control" min="1" value="1">
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <input name="recipes[]" type="text" class="form-control" placeholder="">
            </div>
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <button title="Rezept löschen" type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-div" disabled></button>
            </div>
        </div>
        <button type="button" class="btn btn-default glyphicon glyphicon-plus insert"></button> Rezept hinzufügen!
        <br> <br>
    </div>
    <!-- <button type="button" class="btn btn-success fplan-check">Ernährungsplan prüfen!</button> -->
    <button type="submit" class="btn btn-primary">Ernährungsplan speichern!</button>
</form>