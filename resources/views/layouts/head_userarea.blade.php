<!-- spacer section:testimonial -->
<section id="testimonials-3" class="section" data-stellar-background-ratio="0.5">
<div class="container">
    <div class="row">               
        <div class="col-lg-12">
            <div class="align-center">
                    <div class="testimonial pad-top40 pad-bot40 clearfix">
                        <h5>
                            Nunc velit risus, dapibus non interdum quis, suscipit nec dolor. Vivamus tempor tempus mauris vitae fermentum. In vitae nulla lacus. Sed sagittis tortor vel arcu sollicitudin nec tincidunt metus suscipit.Nunc velit risus, dapibus non interdum.
                        </h5>
                        <br/>
                        <span class="author">&mdash; Jouse Manuel <a href="#">www.jouse-manuel.com</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</section>
<div class="topspace"></div>