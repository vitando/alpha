<div class="panel panel-primary clear">
    <div class="panel-body text-center footer">
        <b> - V I T A N D O - </b>
    </div>
    <div class="panel-footer">
        <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center clear">
                <h3>Trainingspläne nach Kattegorien:</h3>
                <hr>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center clear">
                <h3>Ernährungspläne nach Kattegorien:</h3>
                <hr>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-3 col-lg-3 text-center">
                <img src="/pics/main/logo_v3.png" class="img-responsive" alt="Image">
            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 network">
                <h3>Netzerwerk:</h3>
                <hr>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <a href=""><img src="/pics/main/network/fb_logo.png" class="img-responsive" alt="Image"></a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <a href=""><img src="/pics/main/network/yt_logo.png" class="img-responsive" alt="Image"></a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <a href=""><img src="/pics/main/network/twitter_logo.png" class="img-responsive" alt="Image"></a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <a href=""><img src="/pics/main/network/insta_logo.png" class="img-responsive" alt="Image"></a>
                </div><br><br>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <h3>Kontakt:</h3>
                <hr>
                <b>E-Mail:</b> info@vitando.com<br>
                <b>Telefon:</b> 02233/123456<br>
                <b>Anschrift:</b><br>
                vitabndo uG <br>
                Teststr. 12 <br>
                50354 Hürth

            </div>

            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <h3>Rechtliches:</h3>
                <hr>
                <a href="">AGBs</a><br>
                <a href="">Datenschutz</a><br>
                <a href="">Impressum</a><br>
                <a href="/faq">FAQ</a><br>
            </div>
        </div>
    </div>
</div>