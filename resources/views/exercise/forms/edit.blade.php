<form action="/übung/bearbeiten/{{$exercise->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$exercise->title}}" placeholder="{{$exercise->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="">Kategorie</label><br>
                @foreach($exercises_categories as $exercises_categorie)
                    <span class="form-group btn btn-danger toggleCategorie">
                        <input type="hidden" name="" class="form-control" value="{{$exercises_categorie->id}}">
                       {{$exercises_categorie->title}}
                    </span>
                @endforeach

    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$exercise->description}}</textarea>
    </div>                   
    <div class="form-group col-sm-12">
        <button type="submit" class="btn btn-primary">Speichern!</button>
    </div>                    
    
</form>