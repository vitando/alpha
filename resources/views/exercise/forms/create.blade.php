<form action="/exercise/store" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="">Titel</label>
        <input type="text" class="form-control" name="title" placeholder="" required="required">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required"></textarea>
    </div> 
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label for="">Ziele</label> <br>
                @foreach($aims as $aim)
                    <span class="form-group btn btn-danger toggleAim">
                        <input type="hidden" name="" class="form-control" value="{{$aim->id}}">
                       {{$aim->title}}
                    </span>
                @endforeach 
        </div>  
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label for="">Kategorien</label> <br>
                @foreach($exercises_categories as $exercises_categorie)
                    <span class="form-group btn btn-danger toggleCategorie">
                        <input type="hidden" name="" class="form-control" value="{{$exercises_categorie->id}}">
                       {{$exercises_categorie->title}}
                    </span>
                @endforeach
        </div>   
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button type="submit" class="btn btn-primary">Übung erstellen!</button>
    </div>              
</form>