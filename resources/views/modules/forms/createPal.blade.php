<div class="container">
    @if(! empty($energy))
    <form action="/userarea/pal/create" method="POST" class="form-inline" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                    <label class="" for="">Titel (zum wiedererkennen)</label>
                    <input name="title" type="text" class="form-control" id="">
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                    <label class="" for="">Grundumsatz</label>
                    <input name="energy" type="number" class="form-control" id="" value="{{ $energy }}">        
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                    <label class="" for="">Faktor (PAL)</label>
                    <input name="pal_day" type="number" class="form-control" id="" value="{{ $pal_day }}">            
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                    <label class="" for="">Leistungsumsatz</label>
                    <input name="energy_total" type="number" class="form-control" id="" value="{{ $energy_total }}">            
                </div>
            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Speichern</button>
    </form>
    @endif


    <form action="/userarea/pal/make" method="POST" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="">Geschlecht</label>
            <select name="gender" class="form-control" required="required">
                @if(Auth::user()->gender == 0)
                <option value="0" selected>Männlich</option>
                <option value="1">Weiblich</option>
                @else
                <option value="0">Männlich</option>
                <option value="1" selected>Weiblich</option>
                @endif

            </select>
        </div>
        <div class="form-group">
            <label for="">Körpergewicht in Kg</label>
            <input name="weight" type="number" class="form-control" value="{{Auth::user()->weight}}">
        </div>
        <div class="form-group">
            <label for="">Körpergrösse in cm</label>
            <input name="size" type="number" class="form-control" value="{{Auth::user()->size}}">
        </div>
        <div class="form-group">
            <label for="">Alter in Jahren</label>
            <input name="age" type="number" class="form-control" value="{{Auth::user()->age}}">
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Aktivität</th>
                    <th>Faktor</th>
                    <th>Beispiele</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Liegen oder nur sitzen</td>
                    <td>1,2</td>
                    <td>alte, gebrechliche oder bettlägerige Menschen, Rollstuhlfahrer</td>
                </tr>
                <tr>
                    <td>Überwiegend sitzend</td>
                    <td>1,4 bis 1,5</td>
                    <td>Büroangestellte, Bildschirmarbeit, Lehrer</td>
                </tr>
                <tr>
                    <td>Überwiegend sitzend mit stehenden und gehenden Tätigkeiten</td>
                    <td>1,6 bis 1,7</td>
                    <td>Studenten, Laboranten, Kraftfahrer, Fließbandarbeiter</td>
                </tr>
                <tr>
                    <td>Überwiegend stehende und gehende Tätigkeiten</td>
                    <td>1,8 bis 1,9</td>
                    <td>Verkäufer, Handwerker/Meachaniker, Hausfrauen</td>
                </tr>
                <tr>
                    <td>Anstrengende und körperliche Tätigkeiten</td>
                    <td>2,0 bis 2,4</td>
                    <td>Waldarbeiter, Baurarbeiter, Landwirte, Leistungssportler</td>
                </tr>
            </tbody>
        </table>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pal">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group text-center">
                    <label for="">Zeit</label>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group text-center">
                    <label for="">PAL-Wert</label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pal">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                    <input name="pal[]" type="number" class="form-control pal-hour" id="" min="1" max="24" value="24">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                    <input name="pal[]" type="number" min="0.95" max="2.4" step="0.05" class="form-control">
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary" disabled>Erstellen!</button>
    </form>    
</div>