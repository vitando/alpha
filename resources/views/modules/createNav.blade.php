<section id="team" class="team-section appear clearfix">
<div class="container">

		<div class="row mar-bot10">
			<div class="col-md-offset-3 col-md-6">
				<div class="section-header">
				<div class="wow bounceIn">
				
					<h2 class="section-heading animated" data-animation="bounceInUp">Selbst machen</h2>
					<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit, sed quia non numquam.</p>
				
				</div>
				</div>
			</div>
		</div>

			<div class="row align-center mar-bot40">
				<div class="col-md-3">
				<a href="{{ route('createRecipe') }}">
				<div class="wow bounceIn" data-animation-delay="4.8s">
					<div class="team-member">
						<div class="profile-picture">
							<figure><img src="/img/members3.jpg" alt=""></figure>
							<div class="profile-overlay"></div>
								<div class="profile-social">
									<div class="icons-wrapper">
										<a href="#"><i class="fa fa-facebook"></i></a>
										<a href="#"><i class="fa fa-twitter"></i></a>
										<a href="#"><i class="fa fa-linkedin"></i></a>
										<a href="#"><i class="fa fa-pinterest"></i></a>
										<a href="#"><i class="fa fa-google-plus"></i></a>
									</div>
								</div>
						</div>
						<div class="team-detail">
							<h4>Rezepte</h4>
							<!-- <span>User experiences</span> -->
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-3">
					<a href="{{ route('createExercise') }}">
					<div class="wow bounceIn">
						<div class="team-member">
							<div class="profile-picture">
								<figure><img src="/img/members1.jpg" alt=""></figure>
								<div class="profile-overlay"></div>
									<div class="profile-social">
										<div class="icons-wrapper">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-linkedin"></i></a>
											<a href="#"><i class="fa fa-pinterest"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</div>
									</div>
							</div>
							<div class="team-detail">
								<h4>Übungen</h4>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-md-3">
					<a href="{{ route('createTrainplan') }}">					
					<div class="wow bounceIn">
						<div class="team-member">
							<div class="profile-picture">
								<figure><img src="/img/members2.jpg" alt=""></figure>
								<div class="profile-overlay"></div>
									<div class="profile-social">
										<div class="icons-wrapper">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-linkedin"></i></a>
											<a href="#"><i class="fa fa-pinterest"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</div>
									</div>
							</div>
							<div class="team-detail">
								<h4>Trainingspläne</h4>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-md-3">
					<a href="{{ route('createFoodplan') }}">
					<div class="wow bounceIn">
						<div class="team-member">
							<div class="profile-picture">
								<figure><img src="/img/members2.jpg" alt=""></figure>
								<div class="profile-overlay"></div>
									<div class="profile-social">
										<div class="icons-wrapper">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-linkedin"></i></a>
											<a href="#"><i class="fa fa-pinterest"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</div>
									</div>
							</div>
							<div class="team-detail">
								<h4>Ernährungspläne</h4>
							</div>
						</div>
					</div>
					</a>
				</div>
				
			</div>
				
</div>
</section>