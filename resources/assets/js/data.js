$(function() {
    $( ".magic-food" ).each(function(){
        if ($(this).val().length){
            var id = $(this).val();    
            $(this).parent().empty().append('<input type="text" class="form-control" name="food[]">').find('input').magicSuggest({
                method: 'get',
                data: '/food/find',
                value: [id],
                valueField: 'id',
                displayField: 'name',
                allowFreeEntries: true,
                maxSelection: 1,
                maxSuggestions: 10,
                hideTrigger: true,
                matchCase: true
            });
        }
        else{
            $('.magic-food').magicSuggest({
                method: 'get',
                data: '/food/find',
                valueField: 'id',
                displayField: 'name',
                allowFreeEntries: true,
                maxSelection: 1,
                maxSuggestions: 10,
                hideTrigger: true,
                matchCase: true
            });
        }
    });
});

$(document).on('click', '.insert', function(){
    var insert = $(this).parent().find(".plan-insert").last().clone();
    $(this).parent().find(".plan-insert").last().after(insert);
    $(this).parent().find(".plan-insert").last().show().find('.delete-div').attr("disabled", false);
    $(this).parent().find(".plan-insert").last().find('.insert-with-categorie').last().empty().attr("readOnly", true);
    $(this).parent().find(".plan-insert").last().find('.new-day-id').val('new');
});

$(document).on('change', '.insert-categorie', function(){
    var type = $(this).attr("id");
    var categorie_id = $(this).val();
    var items = '';

    $.ajax({
      url: '/' + type + '/by_categorie',
      data: {categorie_id: categorie_id},
      async: false,
      dataType: 'json',
      success: function (data) {
        $.each(data, function(id, data) {
            if (type == 'recipe') {
                items += '<option value=' + data.recipe.id + '>' + data.recipe.title + '</option>';
            }else{
                items += '<option value=' + data.exercise.id + '>' + data.exercise.title + '</option>';
            }
        });
      }
    });
    $(this).parent().parent().find(".insert-with-categorie").empty().append(items).attr("readOnly", false);
});