$(document).on('click', '.delete-div', function(){
        $(this).parent().parent().remove();
});
$(document).on('click', '.delete-insert', function(){
        $(this).parent().parent().hide().find('.change-day-h').attr('type', 'hidden').val('delete');
    });

$(document).on('click', '.toggleCategorie', function(){
    var input = $(this).find('input');

    if ($(this).hasClass('btn-theme')) {
        $(this).removeClass('btn-theme');
        input.attr('name', ""); 

    }else{
        $(this).addClass('btn-theme');
        input.attr('name', "cat[]"); 
    }
});

$(document).on('click', '.toggleAim', function(){
    var input = $(this).find('input');

    if ($(this).hasClass('btn-theme')) {
        $(this).removeClass('btn-theme');
        input.attr('name', ""); 

    }else{
        $(this).addClass('btn-theme');
        input.attr('name', "aim[]"); 
    }
});

$(document).on('change', '.change-day', function(){
        var day = $(this).val();
        $(this).parent().parent().parent().find('.change-day-h').val(day);
    });

$(document).on('click', '.add-plan-day', function(){
    var trainplanDay = $(this).parent().parent().clone();
    $(this).parent().parent().parent().find('.plan-day').last().after(trainplanDay);
    $(this).parent().find('.delete-div').prop("disabled", false);
    $(this).parent().find('.add-plan-day').prop("disabled", true);
    $(this).parent().parent().parent().find('.plan-day').last().find('.new-day-id').val('new');
});
$(document).on('click', '.add-food', function(){
        var food = $(this).parent().find(".ingredients").last().clone();
        $(this).parent().find(".ingredients").last().after(food);
        $(this).parent().find(".ingredients").last().show().find('.delete-div').prop("disabled", false);
        $(this).parent().find(".ingredients").last().show().find('.change-day-h').attr('type', 'number').attr('value', '1');
        $(this).parent().find(".ingredients").last().find('.new-day-id').val('new');
        $(this).parent().find(".ingredients").last().find('.magic-reset').empty().append('<input type="text" class="form-control" name="food[]">').find('input').magicSuggest({
        method: 'get',
        data: '/food/find',
        valueField: 'id',
        displayField: 'name',
        allowFreeEntries: true,
        maxSelection: 1,
        maxSuggestions: 10,
        hideTrigger: true,
        matchCase: true
    });
});

// --- Kalender shit ---

function userevents() {
    var items = new Array();
    $.ajax({
      url: '/userarea/events/get',
      async: false,
      dataType: 'json',
      success: function (data) {
        $.each(data, function(id, data) {
            items[id] = {date: data.event_date, id: data.event_id, eventID: data.id, type: data.event_type, title: data.event_name};
        });
      }
    });
    return items;
}

$('#cal1').clndr({
    template: $('#swag').html(),
    startWithMonth: moment(),
    weekOffset: 1,
    events: userevents(),
    daysOfTheWeek: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
    clickEvents: {
        click: function(target) {
            console.log(target);
        },
        onMonthChange: function(month) {
          console.log('you just went to ' + month.format('MMMM, YYYY'));
          // makedrag();
          // makedrop();
        }
        },
        doneRendering: function() {
        console.log();
      },
});

// --- Kalender shit ENDE ---