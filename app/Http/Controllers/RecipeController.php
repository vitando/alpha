<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\Aim; 
use App\Models\Food;
use App\Models\Recipe\Recipe;
use App\Models\Recipe\Recipes_cat;
use App\Models\Recipe\Recipes_aim;
use App\Models\Recipe\Recipes_categorie;
use App\Models\Recipe\Recipes_food;

class RecipeController extends Controller
{
    public function create()
    {
        $recipes_categories = Recipes_categorie::all();
        $aims = Aim::all();

        return view('userarea.recipes.create', compact('recipes_categories', 'aims'));
    }

    public function store(Request $request)
    {
        $i                 = 0;
        $amount_total      = 0;
        $energy_kj_total   = 0;
        $energy_kcal_total = 0;
        $protein_total     = 0;
        $fat_total         = 0;
        $sugar_total       = 0;
        $carbs_total       = 0;

        $recipe = new Recipe;

        $recipe->user_id = Auth::id();
        $recipe->title = $request->title;
        $recipe->description = $request->description;

        $recipe->url_vid = 'yolo';
        $recipe->url_pic = 'yolo';

        $recipe->save();

        foreach ($request->cat as $cat) {
            $categorie = new Recipes_cat;
            $categorie->recipe_id = $recipe->id;
            $categorie->recipes_categorie_id = $cat;
            $categorie->save();
        }

        foreach ($request->aim as $aim) {
            $newAim = new Recipes_aim;
            $newAim->recipe_id = $recipe->id;
            $newAim->aim_id = $aim;
            $newAim->save(); 
        }

        foreach ($request->food as $food) {
            if ($i == 0) {
                $food_recipe = new Recipes_food;
                $food_recipe->food_id = $food;
                $food_id = $food;
                $i++;
            }
            else if ($i == 1) {
                $food_recipe->amount = $food;
                $food_recipe->recipe_id = $recipe->id;
                $food_recipe->save();
                $i = 0;

                $amount_total = $amount_total + $food;
                $energy_kj_total = $energy_kj_total + (Food::find($food_id)->energy_kJ / 100 * $food);
                $energy_kcal_total = $energy_kcal_total + (Food::find($food_id)->energy_kcal / 100 * $food);
                $protein_total = $protein_total + (Food::find($food_id)->protein / 100 * $food);
                $fat_total = $fat_total + (Food::find($food_id)->fat_total / 100 * $food);
                $sugar_total = $sugar_total + (Food::find($food_id)->sugars / 100 * $food);
                $carbs_total = $carbs_total + (Food::find($food_id)->carbohydrates_available / 100 * $food);
            }
        }
        
        $recipe->amount = $amount_total;
        $recipe->energy_kj = $energy_kj_total;
        $recipe->energy_kcal = $energy_kcal_total;
        $recipe->protein = $protein_total;
        $recipe->fat = $fat_total;
        $recipe->sugar = $sugar_total;
        $recipe->carbs = $carbs_total;
        $recipe->save(); 

        return redirect()->route('homeRecipes');
    }

    public function edit(Request $request)
    {
        $id = $request->session()->get('id');

        if (empty($id) || $id == ""|| $id == NULL) {
            return back();
        }

        $recipes_categories = Recipes_categorie::all();
        $recipe = Recipe::find($id);

        return view('userarea.recipes.edit', compact('recipe', 'recipes_categories'));
    }

    public function update(Request $request, Recipe $recipe)
    {
        $i                 = 0;
        $amount_total      = 0;
        $energy_kj_total   = 0;
        $energy_kcal_total = 0;
        $protein_total     = 0;
        $fat_total         = 0;
        $sugar_total       = 0;
        $carbs_total       = 0;

        $recipe->update([
            'title'                => $request->title,
            'description'          => $request->description,
        ]);


        foreach ($request->food as $food) {
            if ($i == 0) {
                if (Recipes_Food::find($food)) {
                    $recipe_food = Recipes_food::find($food);
                    $i++;
                }else{
                    $recipe_food = new Recipes_food;
                    $i++;
                }
            }
            else if ($i == 1) {
                $recipe_food->food_id = $food;
                $food_id = $food;
                $i++;
            }
            else if ($i == 2) {
                if ($food == "delete") {
                    $recipe_food->delete();
                    $i = 0;
                }else{
                $recipe_food->amount = $food;
                $recipe_food->recipe_id = $recipe->id;
                $recipe_food->save();
                $i = 0;

                $amount_total = $amount_total + $food;
                
                $energy_kj_total = $energy_kj_total + (Food::find($food_id)->energy_kJ / 100 * $food);
                $energy_kcal_total = $energy_kcal_total + (Food::find($food_id)->energy_kcal / 100 * $food);
                $protein_total = $protein_total + (Food::find($food_id)->protein / 100 * $food);
                $fat_total = $fat_total + (Food::find($food_id)->fat_total / 100 * $food);
                $sugar_total = $sugar_total + (Food::find($food_id)->sugars / 100 * $food);
                $carbs_total = $carbs_total + (Food::find($food_id)->carbohydrates_available / 100 * $food);                
                }

            }
        }
        
        $recipe->update([
            'amount'      => $amount_total,
            'energy_kj'   => $energy_kj_total,
            'energy_kcal' => $energy_kcal_total,
            'protein'     => $protein_total,
            'fat'         => $fat_total,
            'sugar'       => $sugar_total,
            'carbs'       => $carbs_total
        ]);

        return redirect()->route('homeRecipes');
    }

    public function findFood()
    {
        $foods = Food::all(['id', 'name']);
        return $foods->toJson();
    }

    public function findRecipe(Request $request)
    {
        $recipes = Recipes_cat::where('recipes_categorie_id', $request->categorie_id)->with('recipe')->get();
        
        return $recipes->toJson();
    }
}
