<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DateTime;

use App\Models\Aim;
use App\Models\Event;

use App\Models\User\Users_aim;

use App\Models\Recipe\Recipe;

use App\Models\Exercise\Exercise;

use App\Models\Trainplan\Trainplan;

use App\Models\Foodplan\Foodplan;

class UserareaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $foodplans = Auth::user()->foodplans()->get();
        $trainplans = Auth::user()->trainplans()->get();
        $recipes = Auth::user()->recipes()->get();
        $exercises = Auth::user()->exercises()->get();
        $birthDate = Auth::user()->bday;
        if (!empty($birthDate)) {
            $birthday = new DateTime($birthDate);
        }else{
            $birthday = new DateTime();
        }
        $interval = $birthday->diff(new DateTime);
        Auth::user()->age = $interval->y;


        return view('userarea.index', compact('foodplans', 'trainplans', 'recipes', 'exercises'));
    }   
    public function clndr()
    {
    	return view('userarea.clndr.index');
    }   

    public function trainplans()
    {
        $trainplans = Auth::user()->trainplans()->get();

        return view('userarea.trainplans.index', compact('trainplans'));
    }
    public function foodplans()
    {
        $foodplans = Auth::user()->foodplans()->get();

        return view('userarea.foodplans.index', compact('foodplans'));
    }

    public function recipes()
    {
        $recipes = Recipe::where('user_id', Auth::id())->get();

        return view('userarea.recipes.index', compact('recipes'));
    }

    public function profile()
    {
        $aims = Aim::all();
        return view('userarea.profile.index', compact('aims'));
    }

    public function exercises()
    {
        $exercises = Exercise::where('user_id', Auth::id())->get();

        return view('userarea.exercises.index', compact('exercises'));
    }

    

}
