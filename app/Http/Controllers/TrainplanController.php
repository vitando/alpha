<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Aim; 

use App\Models\Trainplan\Trainplan;
use App\Models\Trainplan\Trainplans_aim;
use App\Models\Trainplan\Trainplans_cat;
use App\Models\Trainplan\Trainplans_exercise;
use App\Models\Trainplan\Trainplans_categorie;
use App\Models\Exercise\Exercise;
use App\Models\Exercise\Exercises_categorie;

class TrainplanController extends Controller
{    
    public function create()
    {
        $trainplans_categories = Trainplans_categorie::all();
        $exercises_categories = Exercises_categorie::all();
        $aims = Aim::all();

        return view('userarea.trainplans.create', compact('trainplans_categories', 'exercises_categories', 'aims'));
    }
    
    public function edit(Request $request)
    {
        $id = $request->session()->get('id');

        if (empty($id) || $id == ""|| $id == NULL) {
            return back();
        }

        $aims = Aim::all();
        $trainplans_categories = Trainplans_categorie::all();
        $exercises_categories = Exercises_categorie::all();
        $exercises = Exercise::all();
        $trainplan = Trainplan::find($id);
        $trainplans = Trainplan::where('user_id', Auth::id())->get();

        return view('userarea.trainplans.edit', compact('exercises','trainplans','trainplan', 'exercises_categories', 'aims'));
    }

    public function store(Request $request)
    {
    	$i = 0;
        $duration = 0;
    	$trainplan = new Trainplan;

        $trainplan->user_id = Auth::id();
        $trainplan->title = $request->title;
        $trainplan->description = $request->description;
        $trainplan->duration = $duration;

        $trainplan->save();


        foreach ($request->cat as $categorie) {
        	$cat = new Trainplans_cat;
        	$cat->trainplan_id = $trainplan->id;
        	$cat->trainplans_categorie_id = $categorie;
        	$cat->save();
        }

        foreach ($request->aim as $aim) {
            $newAim = new Trainplans_aim;
            $newAim->trainplan_id = $trainplan->id;
            $newAim->aim_id = $aim;

            $newAim->save(); 
        }

        foreach ($request->exercises as $exercise) {
        	if ($i == 0) {
        		$trainplans_exercises = new Trainplans_exercise;
                $trainplans_exercises->day = $exercise;
        		$i++;
        	}
        	else if ($i == 1) {
        		$trainplans_exercises->exercise_id = $exercise;
        		$i++;
        	}
        	else if ($i == 2) {
        		$trainplans_exercises->sets = $exercise;
        		$i++;
        	}
        	else if ($i == 3) {
        		$trainplans_exercises->reps = $exercise;
        		$i++;
        	}
        	else if ($i == 4) {
        		$trainplans_exercises->rm_value = $exercise;
        		$i++;
        	}
        	else if ($i == 5) {
        		$trainplans_exercises->description = $exercise;
        		$trainplans_exercises->trainplan_id = $trainplan->id;

        		$trainplans_exercises->save();
        		$duration++;
        		$i = 0;
        	}
        }

        $updateDuration = Trainplan::find($trainplan->id);
        $updateDuration->duration = $duration;
        $updateDuration->save();

        return back();
    }

    public function update(Request $request, Trainplan $trainplan)
    {
        $i = 0;
        $trainplan->title = $request->title;
        $trainplan->description = $request->description;
        $trainplan->save();

        foreach ($request->exercises as $exercise) {
            if ($i == 0 && $trainplan->trainplans_exercises()->find($exercise)) {
                $exercise_id = $trainplan->trainplans_exercises()->find($exercise);
                $status = "update";
                $i++;
            }
            else if ($i == 0 && $exercise = 'new') {
                $exercise_id = new Trainplans_exercise;
                $status = "new";
                $i++;
            }
            else if ( $i == 1) {
                if ($exercise == 'delete') {
                    $status = "delete";
                }
                $exercise_id->day = $exercise;
                $i++;
            }
            else if ($i == 2) {
                $exercise_id->exercise_id = $exercise;
                $i++;
            }
            else if ($i == 3) {
                $exercise_id->sets = $exercise;
                $i++;
            }
            else if ($i == 4) {
                $exercise_id->reps = $exercise;
                $i++;
            }
            else if ($i == 5) {
                $exercise_id->rm_value = $exercise;
                $i++;
            }
            else if ($i == 6) {
                $exercise_id->description = $exercise;
                $exercise_id->trainplan_id = $trainplan->id;
                if ($status == "new") {
                    $exercise_id->save();
                    $i = 0;
                }
                else if ($status == "update") {
                    $exercise_id->update();
                    $i = 0;
                }
                else if ($status == "delete") {
                    $exercise_id->delete();
                    $i = 0;
                }

            }
        }

        return redirect()->route('editTrainplan')->with('id', $trainplan->id);
    }
}
