<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        //
    }

    public function update(Request $request, User $user)
    {
        Auth::user()->update([
            'weight' => $request->weight;
            'gender' => $request->weight;
            'size'   => $request->weight;
            'bday'   => $request->bday;
        ]);
        return back();
    }


    public function getEvents()
    {
        $events = Event::where('user_id', Auth::id())->get();
        return $events->toJson();
    }

    public function aim(Request $request)
    {
        $aim = Auth::user()->aim();
        if ($aim->count() == 0) {
            Auth::user()->aim()->create([
                            'user_id' => Auth::id(),
                            'aim_id'  => $request->aim_id,
                            'note'    => $request->note,
                            'weight'  => $request->weight,
                        ]);
        }else{
            $aim->update([
                            'aim_id' => $request->aim_id,
                            'note'   => $request->note,
                            'weight' => $request->weight,
                        ]);

        }
        return back();
    }

    public function foodplan(Request $request)
    {
        $users_foodplan = Auth::user()->usersFoodplans->where('foodplan_id', $request->foodplan_id)->first();

        if ($users_foodplan->count() == 0) {
            Auth::user()->usersFoodplans()->create([
                            'user_id'     => Auth::id(),
                            'foodplan_id' => $request->foodplan_id,
                            'status'      => 'active',
                        ]);
        }else{
            $users_foodplan->update([
                            'status'      => $request->status,
                        ]);

        }
        return back();
    }
}
