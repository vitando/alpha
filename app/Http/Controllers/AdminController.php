<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Recipe\Recipes_Categorie;

use Auth;

use App\Models\Aim;
use App\Models\Exercise\Exercises_categorie;
use App\Models\Trainplan\Trainplans_categorie;
use App\Models\Foodplan\Foodplans_categorie;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$recipes_categories = Recipes_categorie::all();
    	$foodplans_categories = Foodplans_categorie::all();
    	$trainplans_categories = Trainplans_categorie::all();
    	$exercises_categories = Exercises_categorie::all();
    	return view('adminarea.index', compact('recipes_categories','foodplans_categories','trainplans_categories', 'exercises_categories'));
    }

    public function createCategorie(Request $request)
    {
        switch ($request->type) {
            case 'foodplan':
                $new = new Foodplans_categorie;
                break;
            case 'trainplan':
                $new = new Trainplans_categorie;
                break;
            case 'recipe':
                $new = new Recipes_categorie;
                break;
            case 'exercise':
                $new = new Exercises_categorie;
                break;
        }

        $new->title = $request->title;
        $new->description = $request->description;
        $new->scope = $request->scope;
        $new->save();

        return back();
    }
    public function createAim(Request $request)
    {
        $aim = new Aim;
        $aim->title = $request->title;
        $aim->user_id = Auth::id();
        $aim->description = $request->description;
        $aim->fat = $request->fat;
        $aim->carbs = $request->carbs;
        $aim->protein = $request->protein;
        $aim->save();

        return back();
    }
}
