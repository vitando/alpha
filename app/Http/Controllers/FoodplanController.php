<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Aim; 

use App\Models\Foodplan\Foodplan;
use App\Models\Foodplan\Foodplans_aim;
use App\Models\Foodplan\Foodplans_cat;
use App\Models\Foodplan\Foodplans_recipe;
use App\Models\Foodplan\Foodplans_categorie;

use App\Models\Recipe\Recipe;
use App\Models\Recipe\Recipes_Categorie;

class FoodplanController extends Controller
{
    public function create()
    {
        $foodplans_categories = Foodplans_categorie::all();
        $recipes_categories = Recipes_categorie::all();
        $aims = Aim::all();

        return view('userarea.foodplans.create', compact('foodplans_categories', 'recipes_categories', 'aims'));
    }

    public function edit(Request $request)
    {
        $id = $request->session()->get('id');

        if (empty($id) || $id == ""|| $id == NULL) {
            return back();
        }

        $aims = Aim::all();
        $recipes_categories = Recipes_categorie::all();
        $recipes = Recipe::all();
        $foodplan = Foodplan::find($id);

        return view('userarea.foodplans.edit', compact('recipes','foodplan', 'recipes_categories', 'aims'));
    }

	public function store(Request $request)
    {
    	$i = 0;
        $duration = 0;
    	$foodplan = new Foodplan;

        $foodplan->user_id = Auth::id();
        $foodplan->title = $request->title;
        $foodplan->type = 'default';
        $foodplan->description = $request->description;
        $foodplan->duration = $duration;
        $foodplan->save();

        foreach ($request->cat as $categorie) {
        	$cat = new Foodplans_Cat;
        	$cat->foodplan_id = $foodplan->id;
        	$cat->foodplans_categorie_id = $categorie;
        	$cat->save();
        }

        foreach ($request->aim as $aim) {
            $newAim = new Foodplans_Aim;
            $newAim->foodplan_id = $foodplan->id;
            $newAim->aim_id = $aim;

            $newAim->save(); 
        }

        foreach ($request->recipes as $recipe) {
        	if ($i == 0) {
        		$foodplans_recipes = new Foodplans_recipe;
                $foodplans_recipes->day = $recipe;
        		$i++;
        	}
        	else if ($i == 1) {
        		$foodplans_recipes->recipe_id = $recipe;
        		$i++;
        	}
        	else if ($i == 2) {
        		$foodplans_recipes->amount = $recipe;
        		$i++;
        	}
        	else if ($i == 3) {
        		$foodplans_recipes->description = $recipe;
        		$foodplans_recipes->foodplan_id = $foodplan->id;

        		$foodplans_recipes->save();
        		$duration++;
        		$i = 0;
        	}
        }

        $updateDuration = Foodplan::find($foodplan->id);
        $updateDuration->duration = $duration;
        $updateDuration->save();

        return redirect()->route('homeFoodplans');
    }

    public function update(Request $request, Foodplan $foodplan)
    {
        $i = 0;
        $foodplan->update(['title' => $request->title, 'description' => $request->description, 'foodplans_categorie_id' => $request->foodplans_categorie_id]);

        foreach ($request->recipes as $recipe) {
            if ($i == 0 && $foodplan->recipes()->find($recipe)) {
                $foodplan_recipe = $foodplan->recipes()->find($recipe);
                $status = "update";
                $i++;
            }
            else if ($i == 0 && $recipe = 'new') {
                $foodplan_recipe = new Foodplans_recipe;
                $status = "new";
                $i++;
            }
            else if ( $i == 1) {
                if ($recipe == 'delete') {
                    $status = "delete";
                }
                $foodplan_recipe->day = $recipe;
                $i++;
            }
            else if ($i == 2) {
                $foodplan_recipe->recipe_id = $recipe;
                $i++;
            }
            else if ($i == 3) {
                $foodplan_recipe->amount = $recipe;
                $i++;
            }
            else if ($i == 4) {
                $foodplan_recipe->description = $recipe;
                $foodplan_recipe->foodplan_id = $foodplan->id;
                if ($status == "new") {
                    $foodplan_recipe->save();
                    $i = 0;
                }
                else if ($status == "update") {
                    $foodplan_recipe->update();
                    $i = 0;
                }
                else if ($status == "delete") {
                    $foodplan_recipe->delete();
                    $i = 0;
                }

            }
        }

        return redirect()->route('homeFoodplans');
    }

    public function delete($id)
    {
        $foodplan = Foodplan::find($id);
        if ($foodplan->user_id == Auth::id()) {
            $foodplan->delete();
        }
        foreach ($foodplan->foodplans_recipes as $key => $recipes) {
            $recipes->delete();
        }
        return back();
    }

    
}
