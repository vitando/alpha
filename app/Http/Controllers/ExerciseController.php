<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Aim; 

use App\Models\Exercise\Exercise; 
use App\Models\Exercise\Exercises_aim; 
use App\Models\Exercise\Exercises_cat; 
use App\Models\Exercise\Exercises_categorie; 

class ExerciseController extends Controller
{
    public function create()
    {
        $exercises = Exercise::where('user_id', Auth::id())->get();
        $exercises_categories = Exercises_categorie::all();
        $aims = Aim::all();

        return view('userarea.exercises.create', compact('exercises_categories', 'exercises', 'aims'));
    }    

    public function edit(Request $request)
    {
        $id = $request->session()->get('id');

        if (empty($id) || $id == ""|| $id == NULL) {
            return back();
        }

        $exercises_categories = Exercises_categorie::all();
        $exercises = Exercise::where('user_id', Auth::id())->get();
        $exercise = Exercise::find($id);

        return view('userarea.exercises.edit', compact('exercises_categories','exercises','exercise'));
    }
    
    public function store(Request $request)
    {
        $exercise = new Exercise;

        $exercise->user_id = Auth::id();
        $exercise->title = $request->title;
        $exercise->description = $request->description;

        $exercise->save(); 

        foreach ($request->cat as $categorie) {
        	$newCat = new Exercises_cat;
        	$newCat->exercise_id = $exercise->id;
        	$newCat->exercises_categorie_id = $categorie;

        	$newCat->save(); 
        }

        foreach ($request->aim as $aim) {
        	$newAim = new Exercises_aim;
        	$newAim->exercise_id = $exercise->id;
        	$newAim->aim_id = $aim;

        	$newAim->save(); 
        }
        
        return back();
    }

    public function update(Request $request, Exercise $exercise)
    {
        $exercise->title = $request->title;
        $exercise->description = $request->description;
        $exercise->save();

        return redirect()->route('editExercise')->with('id', $exercise->id);
    }

    public function findExercise(Request $request)
    {
        $exercise = Exercises_cat::where('exercises_categorie_id', $request->categorie_id)->with('exercise')->get();
        
        return $exercise->toJson();
    }
}
