<?php

namespace App\Models\Trainplan;

use Illuminate\Database\Eloquent\Model;

use App\Models\Exercise\Exercise;

class Trainplans_exercise extends Model
{
    public function trainplan()
    {
    	return $this->belongsTo(Trainplan::class, 'trainplan_id');
    }
    public function exercise()
    {
    	return $this->belongsTo(Exercise::class);
    }
}
