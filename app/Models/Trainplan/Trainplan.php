<?php

namespace App\Models\Trainplan;

use Illuminate\Database\Eloquent\Model;

use App\Models\Aim;
use App\Models\Exercise\Exercise;

class Trainplan extends Model
{
	public function trainplans_exercises()
	{
		return $this->hasMany(Trainplans_Exercise::class);
	}

	public function trainplans_categories()
	{
		return $this->belongsTo(Trainplans_Categorie::class, 'trainplans_categorie_id');
	}
    
	public function aim()
	{
		return $this->belongsTo(Aim::class, 'aim_id');
	}
	
	public function exercises()
	{
		return $this->hasMany(Exercise::class);
	}

}
