<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\Models\Foodplan\Foodplan;

class Users_foodplan extends Model
{
    public function show()
    {
    	return $this->belongsTo(Foodplan::class, 'foodplan_id');
    }
}
