<?php

namespace App\Models\User;

use App\Models\Aim;

use Illuminate\Database\Eloquent\Model;

class Users_aim extends Model
{
	protected $fillable = [
		'user_id',
		'aim_id',
		'weight',
		'note',
	];

    public function show()
    {
        return $this->belongsTo(Aim::class, 'aim_id');
    }
}
