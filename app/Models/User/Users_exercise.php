<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\Models\Exercise\Exercise;

class Users_exercise extends Model
{
    public function show()
    {
    	return $this->belongsTo(Exercise::class, 'exercise_id');
    }
}
