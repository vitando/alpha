<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\Models\Recipe\Recipe;

class Users_recipe extends Model
{
    public function show()
    {
    	return $this->belongsTo(Recipe::class, 'recipe_id');
    }
}
