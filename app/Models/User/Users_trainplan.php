<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\Models\Trainplan\Trainplan;

class Users_trainplan extends Model
{
    public function show()
    {
    	return $this->belongsTo(Trainplan::class, 'trainplan_id');
    }
}
