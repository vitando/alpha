<?php

namespace App\Models\Exercise;

use Illuminate\Database\Eloquent\Model;

class Exercises_cat extends Model
{
	public function exercise()
	{
		return $this->belongsTo(Exercise::class);
	}
}
