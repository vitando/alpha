<?php

namespace App\Models\Exercise;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
	public function exercise()
	{
		return $this->belongsTo(Exercise::class);
	}
}
