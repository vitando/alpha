<?php

namespace App\Models\Recipe;

use App\Models\Food;

use Illuminate\Database\Eloquent\Model;

class Recipes_food extends Model
{
	public function food()
	{
		return $this->belongsTo(Food::class);
	}
}
