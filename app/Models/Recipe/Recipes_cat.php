<?php

namespace App\Models\Recipe;

use Illuminate\Database\Eloquent\Model;

class Recipes_cat extends Model
{
	public function recipe()
	{
		return $this->belongsTo(Recipe::class);
	}
}
