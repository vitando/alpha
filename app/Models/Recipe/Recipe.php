<?php

namespace App\Models\Recipe;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
	protected $fillable = [
		'recipe_id',
		'amount',
		'food_id',
		'title',
		'energy_kj',
		'energy_kcal',
		'protein',
		'fat',
		'sugar',
		'carbs',
		'description',
	];

	public function foods()
	{
		return $this->hasMany(Recipes_Food::class);
	}
}
