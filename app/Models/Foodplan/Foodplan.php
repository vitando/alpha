<?php

namespace App\Models\Foodplan;

use Illuminate\Database\Eloquent\Model;

class Foodplan extends Model
{
	protected $fillable = [
		'recipe_id',
		'food_id',
		'title',
		'energy_kj',
		'energy_kcal',
		'protein',
		'fat',
		'sugar',
		'carbs',
		'description',
		'recipes_categorie_id'
	];

	public function recipes()
	{
		return $this->hasMany(Foodplans_recipe::class);
	}
}
