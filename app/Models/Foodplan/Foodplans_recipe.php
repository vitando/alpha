<?php

namespace App\Models\Foodplan;

use App\Models\Recipe\Recipe;

use Illuminate\Database\Eloquent\Model;

class Foodplans_recipe extends Model
{
	public function recipe()
	{
		return $this->belongsTo(Recipe::class);
	}
}
