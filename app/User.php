<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function foodplans()
    {
        return $this->hasMany(Models\Foodplan\Foodplan::class);
    }
    public function trainplans()
    {
        return $this->hasMany(Models\Trainplan\Trainplan::class);
    }
    public function exercises()
    {
        return $this->hasMany(Models\Exercise\Exercise::class);
    }
    public function recipes()
    {
        return $this->hasMany(Models\Recipe\Recipe::class);
    }
    public function events()
    {
        return $this->hasMany(Models\Event::class);
    }

    public function usersFoodplans()
    {
        return $this->hasMany(Models\User\Users_foodplan::class);
    }

    public function usersExercises()
    {
        return $this->hasMany(Models\User\Users_exercise::class);
    }

    public function usersTrainplans()
    {
        return $this->hasMany(Models\User\Users_trainplan::class);
    }

    public function usersRecipes()
    {
        return $this->hasMany(Models\User\Users_recipe::class);
    }


    public function aim()
    {
        return $this->hasOne(Models\User\Users_aim::class);
    }
}
