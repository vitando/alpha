<?php

use App\Models\Aim;
use App\Models\Foodplan\Foodplans_categorie;
use App\Models\Trainplan\Trainplans_categorie;
use App\Models\Recipe\Recipes_categorie;
use App\Models\Exercise\Exercises_categorie;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $atzen             = new User;
        $atzen->id         = 1;
        $atzen->name       = 'Atzen';
        $atzen->type       = '3';
        $atzen->email      = 'smolen.art@gmail.com';
        $atzen->password   = '$2y$10$nb4pisxMhSzR6Ix/rQUGeOhwM52/J7Yi.kOTGMuWV.dEfHRqoWRUy';
        $atzen->instatoken = '4531956709.60d2b9f.b6ff075c156c4210b59c28a685e52486';
        $atzen->bday       = '1988-01-22';
        $atzen->gender     = 0;
        $atzen->size       = 167.00;
        $atzen->weight     = 62.00;
        $atzen->save();

        $atzen             = new User;
        $atzen->id         = 2;
        $atzen->name       = 'TeQ';
        $atzen->type       = '3';
        $atzen->email      = 'tconrad@gmx.net';
        $atzen->password   = '$2y$10$4h7miDQvPkElPzh.Oo1QNOevkmDedPMe1i3s1fMIlrpiTB3S6yQEm';
        $atzen->instatoken = '225437757.60d2b9f.29e80898765e4f18ade6ab55ee0064c8';
        $atzen->bday       = '07.11.1987';
        $atzen->gender     = 0;
        $atzen->size       = 186.00;
        $atzen->weight     = 82.00;
        $atzen->save();

        $aim              = new Aim;
        $aim->title       = 'Abnehmen';
        $aim->description = 'Abnehmen';
        $aim->fat         = 20;
        $aim->carbs       = 25;
        $aim->protein     = 55;
        $aim->save();

        $foodplans_categories              = new Foodplans_categorie;
        $foodplans_categories->title       = 'Abnehm Phase';
        $foodplans_categories->description = 'Abnehmen';
        $foodplans_categories->scope       = 0;
        $foodplans_categories->save();

        $trainplans_categories              = new Trainplans_categorie;
        $trainplans_categories->title       = 'Muskelaufbau';
        $trainplans_categories->description = 'Pläne um den Aufbau deiner Muskeln zu stärken!';
        $trainplans_categories->scope       = 0;
        $trainplans_categories->save();

        $recipes_categories              = new Recipes_categorie;
        $recipes_categories->title       = 'Vegan';
        $recipes_categories->description = 'Essen ohne Fleisch oder andere tierische Produkte!';
        $recipes_categories->scope       = 0;
        $recipes_categories->save();

        $exercises_categories              = new Exercises_categorie;
        $exercises_categories->title       = 'Muskelaufbau';
        $exercises_categories->description = 'Übungen um den Aufbau deiner Muskeln zu stärken!';
        $exercises_categories->scope       = 0;
        $exercises_categories->save();



    }
}
