<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title', 100);
            $table->string('type');
            $table->text('description');
            $table->integer('duration');
            $table->timestamps();
        });
        Schema::create('foodplans_recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('foodplan_id')->unsigned();
            $table->integer('recipe_id')->unsigned();
            $table->string('amount');
            $table->string('description')->nullable();
            $table->string('day');
            $table->timestamps();
        });
        Schema::create('foodplans_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('scope')->unsigned();
            $table->timestamps();
        });
        Schema::create('foodplans_cats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('foodplan_id');
            $table->integer('foodplans_categorie_id');
            $table->timestamps();
        });
        Schema::create('foodplans_aims', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('foodplan_id');
            $table->integer('aim_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodplans');
        Schema::dropIfExists('foodplans_recipes');
        Schema::dropIfExists('foodplans_categories');
        Schema::dropIfExists('foodplans_cats');
        Schema::dropIfExists('foodplans_aims');
    }
}
