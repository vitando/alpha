<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('type')->default('1');
            $table->string('email', 191)->unique();
            $table->string('password');
            $table->string('bday')->default('0');
            $table->string('instatoken')->default('0');
            $table->integer('gender')->default('0');
            $table->float('size')->default('0');
            $table->float('weight')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('users_aims', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('aim_id')->unsigned();
            $table->text('note');
            $table->integer('weight')->default('0');
            $table->timestamps();
        });

        Schema::create('users_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('exercise_id')->unsigned();
            $table->integer('rm_value')->default('70');
            $table->integer('train_value')->default('0');
            $table->boolean('status');
            $table->timestamps();
        });

        Schema::create('users_trainplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('trainplan_id')->unsigned();
            $table->boolean('status');
            $table->timestamps();
        });
        Schema::create('users_recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('recipe_id')->unsigned();
            $table->boolean('status');
            $table->timestamps();
        });
        Schema::create('users_foodplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('foodplan_id')->unsigned();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('users_aims');
        Schema::dropIfExists('users_exercises');
        Schema::dropIfExists('users_trainplans');
        Schema::dropIfExists('users_recipes');
        Schema::dropIfExists('users_foodplans');
    }
}
