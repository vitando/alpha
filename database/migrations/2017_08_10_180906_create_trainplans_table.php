<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title', 100);
            $table->text('description');
            $table->integer('duration');
            $table->string('type')->nullable();
            $table->float('price')->default(0);
            $table->timestamps();
        });
        Schema::create('trainplans_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainplan_id')->unsigned();
            $table->integer('exercise_id')->unsigned();
            $table->integer('rm_value');
            $table->integer('sets');
            $table->integer('reps');
            $table->string('description')->nullable();
            $table->text('day');
            $table->timestamps();
        });
        Schema::create('trainplans_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('scope');
            $table->timestamps();
        });
        Schema::create('trainplans_cats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainplan_id');
            $table->integer('trainplans_categorie_id');
            $table->timestamps();
        });
        Schema::create('trainplans_aims', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainplan_id');
            $table->integer('aim_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainplans');
        Schema::dropIfExists('trainplans_exercises');
        Schema::dropIfExists('trainplans_categories');
        Schema::dropIfExists('trainplans_cats');
        Schema::dropIfExists('trainplans_aims');
    }
}
