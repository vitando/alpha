<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->text('description');
            $table->timestamps();
        });
        Schema::create('exercises_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('scope');
            $table->timestamps();
        });
        Schema::create('exercises_cats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('exercise_id');
            $table->string('exercises_categorie_id');
            $table->timestamps();
        });
        Schema::create('exercises_aims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('exercise_id');
            $table->string('aim_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
        Schema::dropIfExists('exercises_categories');
        Schema::dropIfExists('exercises_cats');
        Schema::dropIfExists('exercises_aims');
    }
}
