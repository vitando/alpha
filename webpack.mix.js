let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/clndr.scss', 'public/css');

mix.styles([
		'resources/assets/css/template/isotope.css',
		'resources/assets/js/template/fancybox/jquery.fancybox.css',
		'resources/assets/css/template/bootstrap.css',
		// 'resources/assets/css/template/bootstrap-theme.css',
		'resources/assets/css/template/responsive-slider.css',
		'resources/assets/css/template/animate.css',
		'resources/assets/css/template/font-awesome.css',
		'resources/assets/css/template/overwrite.css',
		'resources/assets/css/template/animate.css',
        'resources/assets/css/template/style.css',

		'resources/assets/css/template/font-awesome.min.css',
		'resources/assets/css/template/default.css',

	    'resources/assets/css/magicsuggest.css',
	    'resources/assets/css/main.css',

	], 'public/css/all.css')

	.scripts([

		'resources/assets/js/template/modernizr-2.6.2-respond-1.1.0.min.js',
		'resources/assets/js/template/jquery.js',
		'resources/assets/js/template/jquery.easing.1.3.js',
	    'resources/assets/js/template/bootstrap.min.js',
		'resources/assets/js/template/jquery.isotope.min.js',
		'resources/assets/js/template/jquery.nicescroll.min.js',
		'resources/assets/js/template/fancybox/jquery.fancybox.pack.js',
		'resources/assets/js/template/skrollr.min.js',
		'resources/assets/js/template/jquery.scrollTo-1.4.3.1-min.js',
		'resources/assets/js/template/jquery.localscroll-1.2.7-min.js',
		'resources/assets/js/template/stellar.js',
		'resources/assets/js/template/responsive-slider.js',
		'resources/assets/js/template/jquery.appear.js',
		'resources/assets/js/template/grid.js',
	    'resources/assets/js/template/main.js',
	    'resources/assets/js/template/wow.min.js',

	    'resources/assets/js/moment.js',
	    'resources/assets/js/underscore.min.js',
	    'resources/assets/js/clndr.js',
	    'resources/assets/js/magicsuggest.js',
	    'resources/assets/js/data.js',
	    'resources/assets/js/form.js',

	], 'public/js/all.js');